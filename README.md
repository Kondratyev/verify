Использование доказателя ACL2 для задания теории чисел с фиксированной точкой
=============================================================================

В данном репозитории содержатся результаты реализации теории,
представленной в следующих статьях:

<https://arxiv.org/abs/1801.00969>

<https://www.mais-journal.ru/jour/article/view/764/577>

Работа частично поддержана грантом РФФИ 17-01-00789

Реализация архитектуры, соответствующей данной теории, представлена в
следующем репозитории:

<https://bitbucket.org/ainoneko/lib_verify/src>

Рассмотрим задание теории для чисел с фиксированной точкой в системе
ACL2. На текущем этапе развития проекта мы задали ту часть теории,
которая не содержит доказательства теоремы о корректности нашего метода
вычисления квадратного корня. В этой части теории задан тип *Val_T*,
операции над типом *Val_T*, тип *Arg_Stp*, функция округления, таблица
предвычисленных аппроксимаций корня.

Теория задана на входном языке системы ACL2. Этот язык представляет собой
аппликативный диалект языка Common Lisp. Далее под языком ACL2 будем
понимать входной язык системы ACL2.

Рассматриваемая теория представлена несколькими файлами. Каждый из них
представляет собой отдельную теорию или модуль. Такой файл в системе
ACL2 называется книгой. Книга может включать в себя другие книги.
Процесс доказательства теории, представленной книгой, называется
сертификацией этой книги. В репозитории представлены книги, успешно
прошедшие локальную сертификацию, то есть они были сертифицированы на
нашем компьютере.

В книге *Val_T-theory* заданы тип *Val_T*, операции над типом
*Val_T*, тип *Arg_Stp*, функция округления. Они заданы в блоках
*encapsulate*. Форма *encapsulate* позволяет задать функции (в том числе
нульместные) как специальные константы, путем наложения на них
ограничений. Эти ограничения задаются с помощью конструкции *defruled* в
блоке *encapsulate*. Но, чтобы использовать такие константы и
ограничения на них в нашей теории, необходимо, чтобы существовала хотя
бы одна соответствующая модель. Поэтому, в таком блоке *encapsulate*
требуется привести пример определения специальных констант. Такие
определения можно задавать в блоке *encapsulate* с помощью конструкции
*(local (defun ... ))*. В ходе сертификации книги проверяется
соответствие таких определений заданным в форме *encapsulate*
ограничениям.

В книге *Val_T-theory* в первой секции *encapsulate* заданы константы
*inf_T*, *sup_T*, *delta_T*, примеры их определения и ограничения ни
них. После этой секции в книге *Val_T-theory* можно найти определение
предиката *Val_T-p* и функции *truncate_T*. Предикат *Val_T-p*
определяет принадлежность аргумента типу *Val_T*. Функция округления
*truncate_T* пытается привести аргумент к типу *Val_T*. Данная функция
используется во второй секции *encapsulate* для задания примеров
определений арифметических операций над типом *Val_T*. В этой секции
заданы данные операции и ограничения на них. После этой секции в книге
*Val_T-theory* можно найти третью секцию *encapsulate*. В ней заданы
константы *eps*, *stp*, примеры их определения и ограничения ни них.
После этой секции в рассматриваемой книге можно найти определение
функций *Arg_Stp-p* и *ceiling_Arg_Stp*. Предикат *Arg_Stp-p*
определяет принадлежность аргумента типу *Arg_Stp*. Функция округления
*ceiling_Arg_Stp* пытается привести аргумент к типу *Arg_Stp*. Данная
функция используется в четвертой секции *encapsulate* для задания
примера определения функции *round-stp*. В этой секции задана данная
функция и ограничения на нее. Кроме того, в книге *Val_T-theory* заданы
теоремы о рассмотренных функциях и вспомогательные определения. Отметим,
что данная книга не зависит от других книг нашей теории.

Таблица предвычисленных аппроксимаций корня должна быть задана в блоке
*encapsulate* как функция, представляющая соответствующее отображение.
Поэтому, необходимо привести пример такой функции. Для этого в книге
*root-linear* приведена функция *root-linear* и теоремы о ней. Отметим,
что функция, определение которой требуется привести в блоке
*encapsulate*, должна для аргумента типа *Arg_Stp* возвращать результат
типа *Val_T*. В отличие от такой функции, *root-linear* – универсальная
функция вычисления верхней оценки квадратного корня. Поэтому, книга
*root-linear* не зависит от книги *Val_T-theory* и от других книг нашей
теории. Так как функция *root-linear* – построение в нашей теории,
нужное для доказательства существования модели, то при ее задании мы не
ставили целью получить эффективный метод вычисления верхней оценки
квадратного корня.

Рассмотрим книгу *root-linear*. В ней можно найти определение
вспомогательной функции *root-linear-aux*. Эта функция находит верхнюю
оценку квадратного корня методом линейного поиска. Аргументами этой
функции служат *x* – число, для которого ищется верхняя оценка
квадратного корня, *y* – число, проверяемое на соответствие верхней
оценке, и *d* – шаг, с которым происходит поиск верхней оценки. Функция
*root-linear-aux* определена рекурсивно так, что каждый ее рекурсивный
вызов можно считать шагом поиска. На каждом шаге поиска к числу,
проверяемому на соответствие верхней оценке, прибавляется значение шага
*d*. Поиск прекращается, когда квадрат числа, проверяемого на
соответствие верней оценки, становится больше *x*. Такое число
соответствует верхней оценке квадратного корня и возвращается в качестве
результата исполнения функции *root-linear-aux*.

В системе ACL2 необходимо доказать тотальность каждой функции, поэтому
теоремы, заданные до определения *root-linear-aux*, нужны для
доказательства теоремы о ее завершимости. После определения
*root-linear-aux* задан ряд теорем и определение функции *root-linear*.
Теоремы *root-linear-aux-upper-bound* и *root-linear-aux-lower-bound*
описывают, какие ограничения на аргументы должны соблюдаться, чтобы
результат исполнения *root-linear-aux* был верхней оценкой, а разница
результата и шага была нижней оценкой. Соблюдение этих условий требуется
для построения корректной таблицы предвычисленных аппроксимаций корня.
Другие теоремы описывают, какие ограничения на аргументы должны
соблюдаться, чтобы результат исполнения *root-linear-aux* принадлежал
типу *Val_T*.

Аргументами функции *root-linear* служат *x* – число, для которого
ищется верхняя оценка квадратного корня, и *d* – шаг, с которым
происходит поиск верхней оценки. Значением функции *root-linear*
является результат вызова функции *root-linear-aux* с аргументами *x*,
*(+  1  d)* и *d* соответственно. Таким образом, значением функции
*root-linear* является результат линейного поиска верхней оценки
квадратного корня для числа *x* с шагом *d*, начиная с числа
*(+  1  d)*. Согласно спецификации нашей задачи таблица
предвычисленных аппроксимаций корня строится для чисел, превосходящих
*1*. Поэтому, поиск начинается с числа *(+ 1 d)*, а не с числа *1*.

После определения *root-linear* задан ряд теорем. Отметим, что число
*(+ 1 d)* удовлетворяет ограничениям на *y*, заданным теоремами
*root-linear-aux-upper-bound* и *root-linear-aux-lower-bound*. Поэтому,
при доказательстве теорем *root-linear-upper-bound* и
*root-linear-lower-bound* используются теоремы
*root-linear-aux-upper-bound* и *root-linear-aux-lower-bound*
соответственно. Теорема *root-linear-upper-bound* означает, что если *x*
больше единицы и *d* больше нуля, то квадрат результата вызова функции
*root-linear* от данных аргументов больше или равен *x*. Таким образом,
результат исполнения *root-linear* от таких аргументов является верхней
оценкой квадратного корня из *x*. Теорема *root-linear-lower-bound*
означает, что если *x* больше единицы и *d* больше нуля, то разница
квадрата результата вызова функции *root-linear* от данных аргументов и
*d* меньше *x*. Таким образом, разница результата исполнения
*root-linear* от таких аргументов и *d* является нижней оценкой
квадратного корня из *x*. Другие теоремы описывают, какие ограничения на
аргументы должны соблюдаться, чтобы результат исполнения *root-linear*
принадлежал типу *Val_T*. Для их доказательства используются некоторые
теоремы о функции *root-linear-aux*.

Книга *fixed-point* задает теорию для чисел с фиксированной точкой,
которую можно использовать при доказательстве теоремы о корректности
нашего метода вычисления квадратного корня. Поэтому, книга *fixed-point*
включает в себя книги *Val_T-theory* и *root-linear*. В книге
*fixed-point* задана функция *root_delta_T*. Ее аргументом служит *x*
– число, для которого ищется верхняя оценка квадратного корня. Значением
функции *root_delta_T* является результат вызова функции *root-linear*
с аргументами *x* и *(delta_T)* соответственно. Таким образом,
значением функции *root_delta_T* является результат линейного поиска
верхней оценки квадратного корня для числа *x* с шагом *(delta_T)*,
начиная с числа *(+ 1 (delta_T))*. Отметим, что число
*(+ 1 (delta_T))* принадлежит типу *Val_T*. Таким образом, поиск
начинается с числа, принадлежащего типу *Val_T*.

После определения *root_delta_T* задан ряд теорем и блок
*encapsulate*. Теоремы *root-linear-upper-bound* и
*root-linear-lower-bound* используются для доказательства теорем
*root_delta_T-bounded* и *root_delta_T-bound* соответственно. Они
означают, что если *v* принадлежит типу *Arg_Stp*, то результат
исполнения *root_delta_T* от аргумента *v* является верхней оценкой, а
разница этого результата и шага *(delta_T)* является нижней оценкой.
Значит, оценка, вычисляемая функцией *root_delta_T*, соответствует
спецификациям таблицы предвычисленных аппроксимаций корня. Другие
теоремы описывают, что если *v* принадлежит типу *Arg_Stp*, то
результат исполнения *root_delta_T* от аргумента *v* принадлежит типу
*Val_T*. Для доказательства данного утверждения используются теоремы о
функции *root-linear* и принадлежность к типу *Val_T* числа, с которого
начинается поиск.

В секции *encapsulate* задана таблица предвычисленных аппроксимаций
корня как функция *root*, представляющая соответствующее отображение.
Также в этой секции для функции *root* задано ограничение
*root-constraint*. Это ограничения означает, что если *v* принадлежит
типу *Arg_Stp*, то результат исполнения *root* от аргумента *v*
принадлежит типу *Val_T*, этот результат является верхней оценкой, а
разница этого результата и шага *(delta_T)* является нижней оценкой.
Для задания примера такой функции в этой секции используется функция
*root_delta_T*. Поэтому, для проверки ограничения *root-constraint*
используются теоремы о функции *root_delta_T*.

Примером отображения, представляющего таблицу предвычисленных
аппроксимаций корня, может служить другая функция. В таком случае можно
создать другую книгу, где определение этой функции не будет зависеть от
конструкций *Val_T-theory*. Тогда можно будет задать новую теорию для
чисел с фиксированной точкой, которая будет основана на другом примере
функции, вычисляющей верхнюю оценку квадратного корня. Сделать это
удобным образом позволяет структура разбиения нашей теории на книги.
